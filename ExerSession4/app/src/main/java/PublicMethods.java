import android.content.Context;
import android.widget.Toast;

/**
 * Created by Acer on 6/12/2017.
 */

public class PublicMethods {
    Context mContext;

    public PublicMethods(Context mContext) {
        this.mContext = mContext;
    }

    public void ShowToast(String message){
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT ).show();
    }
}
