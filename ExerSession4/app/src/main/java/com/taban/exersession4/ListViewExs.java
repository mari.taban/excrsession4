package com.taban.exersession4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import Adapter.SoftListAdapter;

public class ListViewExs extends AppCompatActivity {
    ListView softwareList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_exs);

        softwareList = (ListView) findViewById(R.id.softList);
        String softTitle[] = {
                "Chrome" ,"KM Player" ,"Telegram" ,"Weather"
        };

        SoftListAdapter adp = new SoftListAdapter(this,softTitle);
        softwareList.setAdapter(adp);
    }
}
