package com.taban.exersession4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import Adapter.SoftListAdapter;
import Adapter.SoftObjListAdapter;
import Models.SoftwareModel;

public class SoftListActivity extends AppCompatActivity {
    ListView softList;
    GridView softGrid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soft_list);

        softList = (ListView) findViewById(R.id.softList);
        softGrid = (GridView) findViewById(R.id.softGrid);
        SoftwareModel soft1 = new SoftwareModel("Km player", R.drawable.km, 1);
        SoftwareModel soft2 = new SoftwareModel("Telegram",R.drawable.tel, 2);
        SoftwareModel soft3 = new SoftwareModel("Chrome",R.drawable.chrome, 3);
        SoftwareModel soft4 = new SoftwareModel("Weather",R.drawable.wather, 4);
        List<SoftwareModel> soft = new ArrayList<>();

        soft.add(soft1);
        soft.add(soft2);
        soft.add(soft3);
        soft.add(soft4);

        SoftObjListAdapter adapter = new SoftObjListAdapter(this,soft,false);

//        softList.setAdapter(adapter);
        softGrid.setAdapter(adapter);

    }
}
