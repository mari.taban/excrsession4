package Models;

/**
 * Created by taban on 6/12/2017.
 */

public class SoftwareModel {
    String softName;
    int SoftIcon;
    int id;

    public SoftwareModel(String softName, int softIcon, int id) {
        this.softName = softName;
        SoftIcon = softIcon;
        this.id = id;
    }

    public void setSoftIcon(int softIcon) {
        SoftIcon = softIcon;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSoftName(String softName) {
        this.softName = softName;
    }

    public String getSoftName() {
        return softName;
    }

    public int getSoftIcon() {
        return SoftIcon;
    }

    public int getId() {
        return id;
    }
}
