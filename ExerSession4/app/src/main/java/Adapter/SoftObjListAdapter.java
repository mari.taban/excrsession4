package Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.taban.exersession4.R;

import java.util.List;

import Models.SoftwareModel;

/**
 * Created by taban on 6/12/2017.
 */

public class SoftObjListAdapter extends BaseAdapter {
    Context mContext;
    List<SoftwareModel> models;
    boolean isListView = true;
    public SoftObjListAdapter(Context mContext, List<SoftwareModel> models, boolean isListView) {
        this.mContext = mContext;
        this.models = models;
        this.isListView = isListView;
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public Object getItem(int i) {
        return models.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View rowView2 = LayoutInflater.from(mContext).inflate(R.layout.soft_list_obj, viewGroup, false);
        if (isListView == false)
            rowView2 = LayoutInflater.from(mContext).inflate(R.layout.soft_grid_obj, viewGroup, false);

        ImageView softIcon = (ImageView) rowView2.findViewById(R.id.softIcon);
        TextView softNam = (TextView) rowView2.findViewById(R.id.softName);

        softNam.setText(models.get(i).getSoftName());
        softIcon.setImageResource(models.get(i).getSoftIcon());
        return rowView2;

    }
}
