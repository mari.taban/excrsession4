package Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.taban.exersession4.R;

/**
 * Created by Taban on 6/11/2017.
 */

public class SoftListAdapter extends BaseAdapter {
    Context mContext;
    String softTitle[];

    public SoftListAdapter(Context mContext, String[] softTitle) {
        this.mContext = mContext;
        this.softTitle = softTitle;
    }

    @Override
    public int getCount() {
        return softTitle.length;
    }

    @Override
    public Object getItem(int i) {
        return softTitle[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.soft_list_item, viewGroup, false);
        TextView software = (TextView) rowView.findViewById(R.id.software);
        software.setText(softTitle[i]);
        return rowView;
    }
}
